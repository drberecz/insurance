<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>   
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create an account</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
        <h2>igaz-e hogy admin:${pageContext.request.isUserInRole("ROLE_ADMINKA")} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
    </c:if>

    
<!--  <security:authorize access="hasRole('ROLE_ADMINKA')">  
      <p>Hello BADMINNNN</p>  
      <p>NA megvan a req?: ${requestScope.ezmostR}</p>
      <p>NA megvan a SalesforceID?: ${requestScope.sfid}</p>
</security:authorize>  -->

<security:csrfInput/>    

  </div>    


<p>
    Name+Company: ${visitorInfo.name}
    Accountid: ${visitorInfo.currentAccount.accountIdSalesForce}<br>
    contactId ${visitorInfo.contactIdSalesForce}<br>
    company: ${visitorInfo.currentAccount.company}<br>
    phone: ${visitorInfo.currentAccount.phone}<br>
    address: ${visitorInfo.currentAccount.billingAddress}<br>
</p>


<h1><a href="./policy">REQUEST A NEW POLICY</a></h1>

<p>
<%--<c:if test="${not empty policyList}">--%>
<c:forEach items="${policyList}" var="item">
   item: ${item.createdByUser}<br>
   item: ${item.dateOfCreation}<br>
   item: ${item.ownerAccount}<br>
   item: ${item.vehicleString}<br>
   item: ${item.opportunityName}<br><br>
</c:forEach>
</p>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="${contextPath}/resources/js/bootstrap.min.js"></script>




</body>
</html>
