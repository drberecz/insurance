package bh10.security.util;

import bh10.security.enumerated.HttpConstant;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.stereotype.Component;

@Component
public class CarListCache {

    private final List<String> carList = Collections.synchronizedList(new ArrayList<>());
    private  boolean isCarListObtained = false;
    
    public void downloadContent() {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(HttpConstant.SALESFORCE_CARLIST_URL);

        HttpResponse response = null;
        try {
            response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            //  System.out.println("STATUSCODE: " + statusCode + " ***************************");
            if (!(statusCode >= HttpConstant.HTTP_RESPONSE200 && statusCode < HttpConstant.HTTP_RESPONSE300)) {
                System.out.println("PROBLEM**************SERVER**DOWN");
                return;
            }

            Long systemTimeBefore = System.currentTimeMillis();
            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));
            Long systemTimeAfter = System.currentTimeMillis();
            if (systemTimeAfter - systemTimeBefore > HttpConstant.MAX_TIMEOUT_HTTPCLIENT_MILLISEC) {
                System.out.println("REQUEST TIMEOUT..........");
                return;
            }

            List<String> tempCarList =new ArrayList<>();
            String line = "";
            while ((line = rd.readLine()) != null) {
                tempCarList.add(line);
            }

            if (tempCarList.size() == carList.size()) {
                return;
            }

            synchronized (carList) {
                
                for (int i = carList.size(); i <tempCarList.size(); i++) {
                    carList.add(tempCarList.get(i));
                    
                }
                        isCarListObtained = true;
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public List<String> getCarList() {
        return carList;
    }

    public void addFailSafeItems(){
        carList.add("Lada");
        carList.add("Skoda");
        carList.add("Zastava");
        isCarListObtained = true;
    }

    public boolean isIsCarListObtained() {
        return isCarListObtained;
    }
    
    
}









