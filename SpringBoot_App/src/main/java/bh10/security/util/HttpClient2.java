package bh10.security.util;

import bh10.security.enumerated.HttpConstant;
import bh10.security.model.Account;
import bh10.security.service.AccountService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HttpClient2 {

    private static final int NUM_OF_FIELDS = 4;
    
    private static int accountFileSize = HttpConstant.CONTACT_FILE_INITIAL_SIZE;
    private static Map<String, Account> accountMap = new HashMap<>();

    @Autowired
    AccountService accountService;

    public void getContent() {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(HttpConstant.SALESFORCE_CONFIRMED_ACCOUNT_URL);

        StringBuilder textview = new StringBuilder();
        HttpResponse response = null;
        try {
            response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if (!(statusCode >= HttpConstant.HTTP_RESPONSE200 && statusCode < HttpConstant.HTTP_RESPONSE300)) {
                System.out.println("PROBLEM**************SERVER**DOWN");
                return;
            }

            Long systemTimeBefore = System.currentTimeMillis();
            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));
            Long systemTimeAfter = System.currentTimeMillis();
            if (systemTimeAfter - systemTimeBefore > HttpConstant.MAX_TIMEOUT_HTTPCLIENT_MILLISEC) {
                System.out.println("REQUEST TIMEOUT..........");
                return;
            }

            String line = "";
            while ((line = rd.readLine()) != null) {
                textview.append(line).append("\n");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        if (accountFileSize != textview.length()) {
            accountFileSize = textview.length();
            System.out.println("HEY valtozott a merete: " + accountFileSize);
        }
        
        String temp = textview.toString();
        String[] textviewSplit = temp.split("\n");
        
        for (int i = 0; i < textviewSplit.length; i++) {
            System.out.println("///" + i + ".) " + textviewSplit[i]);
        }

        for (String line : textviewSplit) {
            String[] lineSplitByComma = line.split(",");
            if (lineSplitByComma.length != NUM_OF_FIELDS) {
                System.out.println("PROBLEM**********corrupted ACCOUNT File");
                continue;
            }
            String sfId = lineSplitByComma[0];
            String accountName = lineSplitByComma[1];
            String phone = lineSplitByComma[2];
            String address = lineSplitByComma[3];
            
            
            Account acc = new Account();
            acc.setAccountIdSalesForce(sfId);
            acc.setBillingAddress(address);
            acc.setCompany(accountName);
            acc.setPhone(phone);
            
            

            if (!accountMap.containsKey(sfId)) {
                try {
                    accountService.save(acc);


                    accountMap.put(sfId, acc);
                } catch (Throwable t) {

                }

            }
        }

    }

}

