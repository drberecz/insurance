package bh10.security.util;

import bh10.security.bean.Contact;
import bh10.security.enumerated.HttpConstant;
import bh10.security.repository.UserRepository;
import bh10.security.service.UserServiceImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HttpClient {

    private static final int NUM_OF_FIELDS = 5;
    
    private static int contactIdFileSize = HttpConstant.CONTACT_FILE_INITIAL_SIZE;
    private static Map<String, String> contactIdMap = new HashMap<>();

    @Autowired
    UserServiceImpl userServiceImpl;

    public void getContent() {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(HttpConstant.SALESFORCE_CONFIRMED_CONTACT_URL);

        StringBuilder textview = new StringBuilder();
        HttpResponse response = null;
        try {
            response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
          //  System.out.println("STATUSCODE: " + statusCode + " ***************************");
            if (!(statusCode >= HttpConstant.HTTP_RESPONSE200 && statusCode < HttpConstant.HTTP_RESPONSE300)) {
                System.out.println("PROBLEM**************SERVER**DOWN");
                return;
            }

            Long systemTimeBefore = System.currentTimeMillis();
            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));
            Long systemTimeAfter = System.currentTimeMillis();
            if (systemTimeAfter - systemTimeBefore > HttpConstant.MAX_TIMEOUT_HTTPCLIENT_MILLISEC) {
                System.out.println("REQUEST TIMEOUT..........");
                return;
            }

            String line = "";
            while ((line = rd.readLine()) != null) {
                textview.append(line).append("\n");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        if (contactIdFileSize != textview.length()) {
            contactIdFileSize = textview.length();
        //    System.out.println("HEY valtozott a merete: " + contactIdFileSize);
        }
        
        String temp = textview.toString();
        String[] textviewSplit = temp.split("\n");
        
        for (int i = 0; i < textviewSplit.length; i++) {
        //    System.out.println("///" + i + ".) " + textviewSplit[i]);
        }

        for (String line : textviewSplit) {
            String[] lineSplitByComma = line.split(",");
            if (lineSplitByComma.length != NUM_OF_FIELDS) {
          //      System.out.println("PROBLEM**********corrupted contactId File");
                continue;
            }
            
            Contact contact = new Contact();
            
            contact.setEmail(lineSplitByComma[0]);
            contact.setContactId(lineSplitByComma[1]);
            contact.setAccountId(lineSplitByComma[2]);
            contact.setFirstName(lineSplitByComma[3]);
            contact.setLastName(lineSplitByComma[4]);

            if (!contactIdMap.containsKey(contact.getEmail())) {
                try {
                    userServiceImpl.updateSfContactId(contact);
            //        System.out.println("BERAKTA A DB BE **************" + contact.getEmail());


                    contactIdMap.put(contact.getEmail(), contact.getContactId());
                } catch (Throwable t) {
              //      System.out.println("******MEG ninCS regisztralva*****");
              //      System.out.println(t.getLocalizedMessage());
                }

            }
        }

    }

}
