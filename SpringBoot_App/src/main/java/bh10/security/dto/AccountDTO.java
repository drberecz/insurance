package bh10.security.dto;


public class AccountDTO {

    
    private Long id;

    private String company;

    private String phone;
    
    private String billingAddress;
    
    private String accountIdSalesForce;

    public AccountDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getAccountIdSalesForce() {
        return accountIdSalesForce;
    }

    public void setAccountIdSalesForce(String accountIdSalesForce) {
        this.accountIdSalesForce = accountIdSalesForce;
    }
    
    
    
    
}
