package bh10.security.dto;

import bh10.security.enumerated.PolicyStage;
import java.time.LocalDate;



public class PolicyDTO {


    private Long id;

    private LocalDate dateOfCreation;

    private String createdByUser;

    private String ownerAccount;
    
    private String opportunityName;

    private String vehicleString;
    
    private PolicyStage policyStage;

    public PolicyDTO() {
    }

    
    private PolicyDTO(Builder b){
        
     this.id = b.id;
     this.createdByUser = b.createdByUser;
     this.dateOfCreation= b.dateOfCreation;
     this.opportunityName= b.opportunityName;
     this.ownerAccount = b.ownerAccount;
     this.vehicleString = b.vehicleString;
     this.policyStage = b.policyStage;
        
    }
            
    public static class Builder{
    
    
    private Long id;
    private LocalDate dateOfCreation;
    private String createdByUser;
    private String ownerAccount;
    private String opportunityName;
    private String vehicleString;
    private PolicyStage policyStage;
    
    
    public Builder setPolicyStage(PolicyStage policyStage){
        this.policyStage = policyStage;
        return this;
    }
    
    public Builder setId(Long id){
        this.id = id;
        return this;
    }
    
    public Builder setDateOfCreation(LocalDate dateOfCreation){
        this.dateOfCreation = dateOfCreation;
        return this;
    }
    
    public Builder setCreatedByUser(String createdByUser){
        this.createdByUser = createdByUser;
        return this;
    }
    
    public Builder setOwnerAccount(String ownerAccount){
        this.ownerAccount = ownerAccount;
        return this;
    }
    
    public Builder setOpportunityName(String opportunityName){
        this.opportunityName = opportunityName;
        return this;
    }
    
    public Builder setVehicleString(String vehicleString){
        this.vehicleString = vehicleString;
        return this;
    }
    
    public PolicyDTO build(){
        return new PolicyDTO(this);
    }
    
    
}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getOwnerAccount() {
        return ownerAccount;
    }

    public void setOwnerAccount(String ownerAccount) {
        this.ownerAccount = ownerAccount;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getVehicleString() {
        return vehicleString;
    }

    public void setVehicleString(String vehicleString) {
        this.vehicleString = vehicleString;
    }

    public PolicyStage getPolicyStage() {
        return policyStage;
    }

    public void setPolicyStage(PolicyStage policyStage) {
        this.policyStage = policyStage;
    }
            
     
    
    
    
    
}




































