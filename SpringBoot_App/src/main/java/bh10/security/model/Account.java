package bh10.security.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String company;

    private String phone;
    
    private String billingAddress;
    
    @Column(name="account_Id_SalesForce", unique = true)
    private String accountIdSalesForce;

    public Account() {
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccountIdSalesForce() {
        return accountIdSalesForce;
    }

    public void setAccountIdSalesForce(String accountIdSalesForce) {
        this.accountIdSalesForce = accountIdSalesForce;
    }
    

    
    
    
    
}
