package bh10.security.service;

import bh10.security.dto.AccountDTO;
import bh10.security.model.Account;

public interface IAccountService {


    AccountDTO findBySalesForceId(String pSalesForceAccountId);

    Account save(Account account);
}
