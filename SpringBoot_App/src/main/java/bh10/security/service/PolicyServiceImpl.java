package bh10.security.service;

import bh10.security.dto.PolicyDTO;
import bh10.security.mapper.PolicyMapper;
import bh10.security.model.Policy;
import bh10.security.repository.PolicyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PolicyServiceImpl implements PolicyService{


@Autowired
PolicyRepository policyRepository;

    @Override
    public void save(Policy policy) {
        policyRepository.save(policy);
    }

    @Override
    public List<PolicyDTO> findByOwnerAccountId(String pSalesForceAccountId) {
        List<Policy> entityList = policyRepository.findByOwnerAccountId(pSalesForceAccountId);
        return  PolicyMapper.mapPolicyEntityListToDto(entityList);
    }




    
}
