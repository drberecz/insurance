package bh10.security.service;

import bh10.security.bean.Contact;
import bh10.security.model.Role;
import bh10.security.model.User;
import bh10.security.repository.RoleRepository;
import bh10.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        
        HashSet<Role> roleSet = new HashSet<>();
        roleRepository.findById(3l).ifPresent(roleSet::add);
        user.setRoles(roleSet);
       // user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User updateSfContactId(Contact contact) {

        User user = findByUsername(contact.getEmail());
        user.setContactIdSalesForce(contact.getContactId());
        user.setAccountIdSalesForce(contact.getAccountId());
        user.setFirstName(contact.getFirstName());
        user.setLastName(contact.getLastName());
        return userRepository.save(user);

    }

    @Override
    public String getContactIdForUser(String username) {
        User user = findByUsername(username);
        return user.getContactIdSalesForce();
    }
    
    
    
    
}











