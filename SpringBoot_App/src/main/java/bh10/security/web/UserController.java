package bh10.security.web;

import bh10.security.AuthenticationFacade;
import bh10.security.bean.VisitorInfo;
import bh10.security.dto.AccountDTO;
import bh10.security.dto.PolicyDTO;
import bh10.security.model.Policy;
import bh10.security.service.SecurityService;
import bh10.security.service.UserService;
import bh10.security.validator.UserValidator;
import bh10.security.model.User;
import bh10.security.model.Userke;
import bh10.security.repository.UserkeDAO;
import bh10.security.service.AccountService;
import bh10.security.service.PolicyService;
import bh10.security.util.CarListCache;
import bh10.security.util.HttpClientPost;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

//jdbc:h2:mem:testdb
@Controller
@SessionAttributes("visitor")
public class UserController {

    private int count = 3;

    
    @Autowired
    private CarListCache carListCache;
    
    @Autowired
    private VisitorInfo visitorInfo;

    @Autowired
    private UserService userService;

    @Autowired
    private PolicyService policyService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private UserkeDAO userkeDAO;

    @Autowired
    private AccountService accountService;

    @GetMapping("/regelj")
    public String getBigLogic() {

        return "regelj";
    }

    @GetMapping("/policy")
    public String getPolicyRequestForm(Model model) {

        model.addAttribute("policyForm", new Policy());

        if (!carListCache.isIsCarListObtained()){
            carListCache.addFailSafeItems();
        }
        List<String> carList = carListCache.getCarList();
        model.addAttribute("carListSelection", carList);
        
        
        return "policyrequest";
    }

    @PostMapping("/policy")
    public String validatePolicyForm(@ModelAttribute("policyForm") Policy policyForm, BindingResult bindingResult) {

        String contactId = userService.getContactIdForUser(securityService.findLoggedInUsername());
        User user = userService.findByUsername(securityService.findLoggedInUsername());

        Policy policy = new Policy.Builder()
                .setCreatedByUser(contactId)
                .setDateOfCreation(LocalDate.now())
                .setOwnerAccount(user.getAccountIdSalesForce())
                .setvehicleString(policyForm.getVehicleString())
                .setOppName(policyForm.getOpportunityName())
                .build();

        policyService.save(policy);
        HttpClientPost.sendPolicyContent(policy);


        return "redirect:welcome";
    }

    @PostMapping("/regelj")
    public String getBigLogicPost(Model model, Userke userke) {

        System.out.println(userke.toString());

        userkeDAO.save(userke);
        model.addAttribute(userke);
        return "reg-done";
    }

    @ModelAttribute("visitor")
    public String getVisitor(SessionStatus sessionStatus, ModelMap modelMap) {

        String getterStr = authenticationFacade.getAuthentication().getName();
        return (!getterStr.isEmpty()) ? getterStr : "ANONYMOUS";
    }

    @GetMapping("/test")
    public String testIt(Model model, ModelMap modelMap) {

        model.addAttribute("testt", "SUCCESS-COUNT: " + count++);
        return "testit";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {

        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        return "login";
    }

    @GetMapping("/test2")
    public String testforward(Model model) {

        model.addAttribute("carList", carListCache.getCarList());
        
        
        
        User user = userService.findByUsername(securityService.findLoggedInUsername());
        String accId = user.getAccountIdSalesForce();
        List<PolicyDTO> policyList = policyService.findByOwnerAccountId(accId);

        
        return "test";
    }

    @GetMapping({"/", "/welcome"})
    public String welcome(@ModelAttribute("visitor") String visitor,  Model model,HttpServletRequest request) {
    public String welcome(Model model, HttpServletRequest request) {

        Principal principal = request.getUserPrincipal();
        String username = principal.getName();
        System.out.println(username);

        List<Account> aList = accountRepository.findAll();
        User user = userService.findByUsername(securityService.findLoggedInUsername());

        String accId = user.getAccountIdSalesForce();
        List<PolicyDTO> policyList = policyService.findByOwnerAccountId(accId);    
        model.addAttribute("policyList", policyList);
        
        try {
               if (visitorInfo.getName() == null || visitorInfo.getName().isEmpty()) {

            AccountDTO currentAccount = accountService.findBySalesForceId(user.getAccountIdSalesForce());
            visitorInfo = new VisitorInfo();
            visitorInfo.setName(user.getFirstName() + " " + user.getLastName() + currentAccount.getCompany());
            visitorInfo.setContactIdSalesForce(user.getContactIdSalesForce());
            visitorInfo.setCurrentAccount(currentAccount);

            AccountDTO testDto = visitorInfo.getCurrentAccount();


            model.addAttribute("visitorInfo", visitorInfo);
        } catch (Throwable e) {
            e.printStackTrace();
        }


        return "welcome";
    }
}
