package bh10.security.web;

import bh10.security.bean.VisitorInfo;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/visitor")
public class VisitorController {

    @Autowired
    private VisitorInfo visitorInfo;

    
    @GetMapping("/check")
    public String appHandler(Model model) {
        if (visitorInfo.getName() == null) {
            return "visit-main";
        }
        model.addAttribute("visitor", visitorInfo);
        visitorInfo.increaseVisitorCounter();
        return "visit-app";
    }

    @PostMapping("/add")
    public String visitorHandler(String name) {
        visitorInfo.setName(name);
        visitorInfo.setContactIdSalesForce("CSULOKANDras********");
        visitorInfo.setFirstVisitTime(LocalDateTime.now());
        return "redirect:/visitor/check";
    }
}
