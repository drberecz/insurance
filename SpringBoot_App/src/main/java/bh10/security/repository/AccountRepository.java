package bh10.security.repository;

import bh10.security.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Long>{


    @Query("Select a FROM Account a WHERE a.accountIdSalesForce=:pSalesForceAccountId") 
    public Account findBySalesForceId(@Param("pSalesForceAccountId") String pSalesForceAccountId);
    
}
