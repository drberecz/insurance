package bh10.security.bean;

import bh10.security.dto.AccountDTO;
import bh10.security.model.Account;
import java.io.Serializable;
import java.time.LocalDateTime;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;


@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class VisitorInfo implements Serializable {
  private String name;
  private int visitCounter;
  private LocalDateTime firstVisitTime;
  
  private AccountDTO currentAccount;
  private String contactIdSalesForce;

    public AccountDTO getCurrentAccount() {
        return currentAccount;
    }

    public void setCurrentAccount(AccountDTO currentAccount) {
        this.currentAccount = currentAccount;
    }

  
  
  
  
    public String getContactIdSalesForce() {
        return contactIdSalesForce;
    }

    public void setContactIdSalesForce(String contactIdSalesForce) {
        this.contactIdSalesForce = contactIdSalesForce;
    }
  

    public VisitorInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVisitCounter() {
        return visitCounter;
    }

    public void setVisitCounter(int visitCounter) {
        this.visitCounter = visitCounter;
    }

    public LocalDateTime getFirstVisitTime() {
        return firstVisitTime;
    }

    public void setFirstVisitTime(LocalDateTime firstVisitTime) {
        this.firstVisitTime = firstVisitTime;
    }

    public void increaseVisitorCounter() {
        ++visitCounter;
    }

  
  
  
}
