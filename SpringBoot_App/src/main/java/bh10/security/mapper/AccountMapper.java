package bh10.security.mapper;

import bh10.security.dto.AccountDTO;
import bh10.security.model.Account;


public class AccountMapper {


public static AccountDTO mapAccountEntityToDto(Account account){
    
    AccountDTO dto = new AccountDTO();
    
    dto.setAccountIdSalesForce(account.getAccountIdSalesForce());
    dto.setBillingAddress(account.getBillingAddress());
    dto.setCompany(account.getCompany());
    dto.setId(account.getId());
    dto.setPhone(account.getPhone());
    
    return dto;
    
}
    
}
