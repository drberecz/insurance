public class OpportunityHandler {
    
    public static void createVehiclesFromOpportunityField(List<Opportunity> triggerNew){
        
    final String URL = 'http://www.hidegver.nhely.hu/sf/ApaPhp2/opportunity.php?todelete=';
    String messagePhp = '';
    String carMake ='';
    Integer carYear = 0;
    Integer quantity = 0;
        
    List<Vehicle__c> vehiclesToInsert = new List<Vehicle__c>();
    
    for (Opportunity opp : triggerNew){
        
        Id oppId = opp.Id;
        Id accId = opp.AccountId;
        
        messagePhp +=accId+',';
        
        String vehicleStr = opp.Kocsik__c;
        System.debug('Vehicle String: ' + vehicleStr);
        
        if(!String.isEmpty(vehicleStr)){
            String[] vehicleListSplit = vehicleStr.split('\\|');
            for(String line: vehicleListSplit){
                 System.debug('SplitBy| ' + line);
                 String[] lineSplit = line.split('\\/'); 
                 carMake = lineSplit.get(0);
                 carYear = Integer.valueOf(lineSplit[1]);
                 quantity= Integer.valueOf(lineSplit[2]);
                 Vehicle__c newVehicle = new Vehicle__c();
                 
                newVehicle.CarType__c = carMake;
                newVehicle.CarYear__c = carYear;
                newVehicle.Quantity__c= quantity;
                newVehicle.Opportunity__c = oppId;
                    
                vehiclesToInsert.add(newVehicle);
                
               
            }
        }
        
    }
   try{
   		insert(vehiclesToInsert);
     //ezt at kene tenni scheduled class-ba 
   // HttpContactInsert.sendContents(messagePhp, URL);
        System.debug('****************Opportunities to be deleted from Php File' + messagePhp);
       
     //  Synchro__c synchItem = new Synchro__c(name = 'delete this', ToBeDeletedFromRelay__c = messagePhp);
     //  insert synchItem;
       
    }catch(DmlException e){
        System.debug('failed');
        System.debug(e.getCause());
    }
	

    }
    
    
    public static void opportunityStageChangedToQuote(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap ){
        
    List<Quote>quoteList=new List<Quote>();
    List<QuoteLineItem> qliList =new List<QuoteLineItem>();
    List<PriceBook2> pb = [SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true LIMIT 1];
        List<Opportunity> opp = [SELECT Id, Account.Name, Name, Contractual_period__c FROM Opportunity WHERE Id IN:triggerNew AND StageName=:'Proposal/Price Quote'];
       String accountName = '';
        String period='';
        String emailContact='';
        for(Opportunity o: opp){
           accountName = o.Account.Name;
            period = o.Contractual_period__c;
        }
        emailContact = getContactEmail(accountName);
      
    for(Opportunity op:opp){
        
        if((Trigger.IsUpdate)&&(triggerOldMap.get(op.Id).StageName=='Prospecting')){
          Quote q = new Quote(
            Description=System.today()+'', 
            Name=op.Name, 
          	OpportunityId = op.Id,
            Email = emailContact,
          	PriceBook2Id = pb[0].Id	
          );
            
   	      quoteList.add(q);  
   	      insert quoteList;
            
          Id quoteId = q.Id;
            System.debug('*'+quoteId);
          List<OpportunityLineItem> oli = [SELECT Id,PricebookEntryId,Quantity,UnitPrice,Product2Id FROM OpportunityLineItem WHERE  OpportunityId =:op.Id];
            for (OpportunityLineItem o: oli){
                QuoteLineItem qli = new QuoteLineItem();            
                qli.OpportunityLineItemId = o.Id;
                qli.QuoteId = quoteId;//[SELECT Id FROM Quote WHERE OpportunityId=: op.Id LIMIT 1].Id;
                qli.Quantity = o.Quantity;
                qli.UnitPrice = o.UnitPrice;
                qli.Product2Id = o.Product2Id;
                qli.PricebookEntryId = o.PricebookEntryId;         
                qli.Discount = calculateDiscount(period);
                qliList.add(qli);
        	}
   
        } 
        
        try{
            insert qliList;}
        catch(Exception ex){ 
           System.debug(''+ex);}
	}

    }
    public static void opportunityStageChangedToWin(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap ){
        /*
    List<Contract>contractList=new List<Contract>();
  
    List<PriceBook2> pb = [SELECT Id FROM Pricebook2 WHERE IsActive = true  LIMIT 1];
        List<Opportunity> opp = 
            [SELECT Id, Account.Name,Account.Id, Name, Contractual_period__c, Contract_Ends__c,
             Contract_start__c,CreatedById, Pricebook2Id
             FROM Opportunity
             WHERE Id IN:triggerNew AND StageName=:'Closed Won'];
       String accountName = '';
        Id accId;
        String period='';
        String emailContact='';
        for(Opportunity o: opp){
           accountName = o.Account.Name;
            accId = o.Account.Id;
            period = o.Contractual_period__c;
            System.debug('* accId: '+accId);
        }
       // emailContact = getContactEmail(accountName);
      System.debug('* before opp');
    for(Opportunity op:opp){
         System.debug('* after opp');
        if((Trigger.IsUpdate)&&(triggerOldMap.get(op.Id).StageName=='Proposal/Price Quote')){
          Contract q = new Contract();
            q.Name= accountName+'-'+System.today();
            q.StartDate = op.Contract_start__c;
             q.ContractTerm =(calculateMonth(op.Contractual_period__c));
          
              q.PriceBook2Id =op.Pricebook2Id;
            
            q.Status = 'Draft';
            q.AccountId = op.Account.Id;
           //   q.ShippingAddress ='aaa';
             // q.BillingAddress='aaa';
              //q.AccountIdStatus='Draft';

    
       
   	      contractList.add(q);  
   	   System.debug('* add to contact ');
              System.debug('* add to contact qqq: '+q);
              System.debug('***** try insert contract ? '+contractList.size());
        	}
   
        }  try{
               System.debug('*try insert contract ? '+contractList.size());
            
            insert contractList;}
        catch(Exception ex){ 
          
System.debug('*catch insert contract');           System.debug('*'+ex);
    }
	
*/
    }
    public static String getContactEmail(String accountName){
        String emailContact='';
        List<Contact> contactList = [SELECT id, email, Account.Name FROM Contact WHERE Account.Name=:accountName LIMIT 1];
        for(Contact c: contactList){
            emailContact = c.email;
            System.debug('emailContact'+emailContact);
        }
       
        return emailContact;
    }
    public static Integer calculateDiscount(String period){
        Switch on period {
        when '3 MONTHS'{
           return 0;
        }
        when '6 MONTHS'{
            return 3;
        }
        when '12 MONTHS'{
           return 5;
        }
        when else{
           return 5;
        }
       }
    }
     public static Integer calculateMonth(String period){
        Switch on period {
        when '3 MONTHS'{
           return 3;
        }
        when '6 MONTHS'{
            return 6;
        }
        when '12 MONTHS'{
           return 12;
        }
        when else{
           return 12;
        }
       }
    }
}