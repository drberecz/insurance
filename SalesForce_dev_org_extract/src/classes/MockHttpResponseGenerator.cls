@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {


    global HTTPResponse respond(HTTPRequest req) {

        System.assertEquals(String.valueOf(URLS__c.UrlContactTrigger__c), req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/csv');
        res.setBody('Account ID,Close Date,Kocsik,Name,Stage');
        res.setStatusCode(200);
        return res;
    }
}