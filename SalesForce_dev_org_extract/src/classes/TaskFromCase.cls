public class TaskFromCase {

    public static final String NEW_TASK = 'Uj task:';
    public static final String TASK_STATUS_NOT_STARTED = 'Not Started';    
 
    
    public static void createTasks(List<Case> caseList) {

        List<Task> taskList = new List<Task>();

        List<String> casePriorityLevels = getPickListValuesIntoList();

        for (Case c : caseList) {

            Task task = new Task();

            task.WhatId = c.Id;
            task.Description = c.Description;
            task.OwnerId = c.OwnerId;
            task.Priority = c.Priority;
            task.Subject = NEW_TASK + c.Subject;
            task.Status = TASK_STATUS_NOT_STARTED;
            Date targetDate = System.today();
            targetDate = targetDate.addDays(calculateDeadlineAccordingToPriority(c.Priority));
            task.ActivityDate = targetDate;

            taskList.add(task);
        }

        insert taskList;


    }

    private static Integer calculateDeadlineAccordingToPriority(String priority){
        
        switch on priority {
            
            when 'Low'{
                return ConstTask.DEADLINE_DAYS_LOW_PRIORITY; 
            }
            when 'Medium'{
                return ConstTask.DEADLINE_DAYS_MEDIUM_PRIORITY;
            }
            when 'High'{
                return ConstTask.DEADLINE_DAYS_HIGH_PRIORITY;
            }
        }
        return ConstTask.DEADLINE_DAYS_DEFAULT;
        
    }
    
    
    
    public static List<String> getPickListValuesIntoList() {
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Priority.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    

}