public class ConstTask {
    
    public static Integer DEADLINE_DAYS_LOW_PRIORITY = 14;
	public static Integer DEADLINE_DAYS_MEDIUM_PRIORITY = 7;
	public static Integer DEADLINE_DAYS_HIGH_PRIORITY = 4;    
	public static Integer DEADLINE_DAYS_DEFAULT = 7;       

    public static final String NEW_TASK = 'Uj task:';
    public static final String TASK_STATUS_NOT_STARTED = 'Not Started';    
    public static final String TASK_PRIORITY_LOW = 'Low';    
    public static final String TASK_PRIORITY_MEDIUM = 'Medium';    
    public static final String TASK_PRIORITY_HIGH = 'High';  


}