public class OpportunityUtil {
    
    final static String FLEET = 'Flotta CASCO';
    
    public static void assignPriceBook(List<Opportunity> scope){
        
        Pricebook2 priceBook = [SELECT  Id FROM Pricebook2 WHERE Name = 'Standard Price Book'];
        for(Opportunity op: scope)
            op.Pricebook2Id = priceBook.Id;
    }
    
   public static void createDefaultProduct(Opportunity[] scope, String productName){        
      String queryString = FLEET+' '+productName;
      Product2 assignedProduct = [SELECT Id FROM Product2 WHERE Name =:queryString LIMIT 1];
//for(Opportunity op:scope)
	//    op.Product2Id = assignedProduct.Id;  
 
        //Brand__c brandy =[SELECT name FROM Brand__c WHERE Brand__c.name =:carMake LIMIT 1];
         Product2 p = new Product2();
             p.Id = assignedProduct.Id;          
             p.Name = queryString;
           //  oli.Quantity = quantityParam;
          //   oli.Description = carYear+''; //unused
           //  oli.Product2Id = assignedProduct.Id;
           //  oli.CarType__c = brandy.Name; //Brand
            // oli.PricebookEntryId = pbs.get(op.name).id;
             
    	//opportunityLineItemList.add(oli);
   }
    
    public static void parseOpportunity(List<Opportunity> scope){
         List<OpportunityLineItem> oppLineList = new List<OpportunityLineItem>();
        String messagePhp = '';
    
    for (Opportunity opp : scope){
        
        Id oppId = opp.Id;
        Id accId = opp.AccountId;
        
        messagePhp +=accId+',';
        
        String opportunityLineString = opp.Kocsik__c;
        System.debug('*******kocsi ' + opportunityLineString);
        
   
            
        if(!String.isEmpty(opportunityLineString)){
            String[] opportunityLineListSplit = opportunityLineString.split('\\|');
            for(String line: opportunityLineListSplit){
                System.debug('*********splitBy| ' + line);
                String[] lineSplit = line.split('\\/'); 
                String carMake = lineSplit.get(0);
                Integer carYear = Integer.valueOf(lineSplit[1]);
                Integer quantity= Integer.valueOf(lineSplit[2]);
                
           //     OpportunityLineItem newOpportunityLineItem = new OpportunityLineItem();
             //   Brand__c brandy =[SELECT name FROM Brand__c WHERE Brand__c.name =:carMake LIMIT 1];
              //  newOpportunityLineItem.CarType__c = brandy.Name; //Brand
             //   newOpportunityLineItem.Description = carYear+''; //unused
            //    newOpportunityLineItem.Quantity= quantity; 
               // newOpportunityLineItem.OpportunityId = oppId;
                
            //    OpportunityLineItem oppLineItem = new OpportunityLineItem();
			//	oppLineItem.quantity=quantity;               
                    
             //   oppLineList.add(newOpportunityLineItem);                             
            }
        }
    }
    
    
    }
}