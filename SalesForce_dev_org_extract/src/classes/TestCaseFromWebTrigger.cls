@isTest
public class TestCaseFromWebTrigger {

    
    @isTest
    public static void is_Task_Created(){
        
        List<AggregateResult> aggrBefore= [SELECT COUNT(Id) numOfTasks FROM Task];
        Integer numOfTasksBefore = (Integer)aggrBefore.get(0).get('numOfTasks');
            
        Case c = new Case();
        c.Status = 'New';
        c.Origin = 'Web';
        insert c;
  
        List<AggregateResult> aggrAfter= [SELECT COUNT(Id) numOfTasks FROM Task];
        Integer numOfTasksAfter = (Integer)aggrAfter.get(0).get('numOfTasks');    
        
        System.assertEquals(1, numOfTasksAfter-numOfTasksBefore);
        
    }
    
}