@isTest(seeAllData = true)
public class TestFindPriceBookEntry {
    private static final String LASTNAME = 'Kis1';
    private static final String CONTACT_EMAIL = 'bruteforcebh10@gmail.com';
    private static final String CONTACT_NAME = 'Andras';
    private static final String TEST_ACCOUNT_NAME = 'Kis és Tsa. Bt.1';
    private static final String INPUT_STRING_CONTAINING_VEHICLES = 'AUDI Q7 3.0 TDI quattro DPF/2017/5|BMW HP2 MEGAMOTO/2016/1|AUDI Quattro 2.2 20V/2018/1';
    private static final String INITIAL_OPPORTUNITY_STAGE = 'Prospecting';
    private static final Integer EXPECTED_NUM_OF_NEW_VEHICLE_ENTRIES = 3;




@isTest
    public static void testOpportunityStageFromProspecting_OpportunityStateChangesToQuote_ReturnGivenNumberOfVehiclesFromOpportunity(){

        List<AggregateResult> vehicleListBefore = [SELECT Count(Id) numOfVs FROM Vehicle__c];
        Integer numOfVehiclesBefore = (Integer)vehicleListBefore.get(0).get('numOfVs');

        Opportunity o =  TestFindPriceBookEntry.testAccountContactOpportunityFactory();

        List<AggregateResult> vehicleListafter = [SELECT Count(Id) numOfVs FROM Vehicle__c];
        Integer numOfVehiclesAfter = (Integer)vehicleListafter.get(0).get('numOfVs');


        System.assertEquals(EXPECTED_NUM_OF_NEW_VEHICLE_ENTRIES, numOfVehiclesAfter - numOfVehiclesBefore);





}
    public static Opportunity testAccountContactOpportunityFactory(){
         Account account = new Account(Name=TEST_ACCOUNT_NAME);
      	 try{
              insert account;}
       	 catch(Exception ex){ System.debug('*'+ex);}
         Contact contact = new Contact(FirstName = CONTACT_NAME, LastName = LASTNAME, Email =CONTACT_EMAIL, AccountId = account.Id);
         insert contact;
         Opportunity opportunity = new Opportunity(Name=account.Name + TEST_ACCOUNT_NAME,
                                       StageName=INITIAL_OPPORTUNITY_STAGE,
                                       Kocsik__c=INPUT_STRING_CONTAINING_VEHICLES,
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=account.Id);
        insert opportunity;
        return opportunity;

    }
}