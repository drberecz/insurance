public class FindPriceBookEntry {

    final static String DEFAULT_INSURANCE_PRODUCT = 'Flotta CASCO ';

    public static void assignPriceBookEntry(List<Opportunity> opportunityList, String carType, Integer carYear, Integer quantityFromOpportunity) {

        Map<string, PricebookEntry> priceBooksMap = new Map<String, PricebookEntry>();
        pricebook2 priceBook = [SELECT id FROM pricebook2 WHERE name = 'Standard Price Book'];

        List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();

        String productName = DEFAULT_INSURANCE_PRODUCT + carType;
        Product2 assignedProductToVehicle = [SELECT Id,Name FROM Product2 WHERE Name = :productName];
       
        String productPriceBookEntryName;
        Map<Id, String> opportunityIdProductNameMap = new Map<Id, String>();
     
        Map<String,Vehicle__c> vehiclePriceBookEntryMap = new Map<String,Vehicle__c>();
        for (Opportunity op : opportunityList) {
            List<Vehicle__c> vehicleList = [SELECT Id, Opportunity__r.Id, CarType__c,PriceFactorByYear__c, 
                                            CarYear__c, Quantity__c FROM Vehicle__c WHERE Opportunity__r.Id = :op.Id];
            for (Vehicle__c v : vehicleList) {
        
               
               v.PriceFactorByYear__c = VintageCalculator.getPricefactorFromCache( Integer.valueOf(v.CarYear__c));
                
                productPriceBookEntryName = DEFAULT_INSURANCE_PRODUCT + CarType;
                priceBooksMap.put(productPriceBookEntryName, null);
                opportunityIdProductNameMap.put(op.Id, productPriceBookEntryName);
             
                vehiclePriceBookEntryMap.put(productPriceBookEntryName, v);
            }
        }

        for (PricebookEntry currentPriceBookEntry : [
                SELECT id, unitprice, product2id, product2.Name
                FROM PricebookEntry
                WHERE product2.Name IN :priceBooksMap.keyset()
                AND pricebook2id = :priceBook.id
        ]) {
            priceBooksMap.put(currentPriceBookEntry.product2.Name, currentPriceBookEntry);
        }
        for (Opportunity op : opportunityList) {

            OpportunityLineItem ol = new OpportunityLineItem();
            ol.opportunityId = op.Id;
            ol.unitprice = priceBooksMap.get(opportunityIdProductNameMap.get(op.Id)).unitprice;
            ol.quantity = quantityFromOpportunity;
            ol.pricebookentryid = priceBooksMap.get(opportunityIdProductNameMap.get(op.Id)).id;            
            
            Vehicle__c currentVehicle =vehiclePriceBookEntryMap.get(opportunityIdProductNameMap.get(op.Id));
            ol.unitprice *=  currentVehicle.PriceFactorByYear__c;
 
            opportunityLineItemList.add(ol);
        }
        insert opportunityLineItemList;
     
    }


}