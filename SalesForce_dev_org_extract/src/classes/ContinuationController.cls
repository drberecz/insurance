public with sharing class ContinuationController {
      
    public String dummy {get;set;}
    
    final String URL = '...';

    public String requestLabel;

    private static final String LONG_RUNNING_SERVICE_URL = 
        '...';


    public Object startTest() {

        
        List<Synchro__c> opportunitiesFromWeb = [SELECT Id, Name, ToBeDeletedFromRelay__c FROM Synchro__c ORDER BY Name]; 
        
        if(opportunitiesFromWeb.isEmpty()){
            return null;
        }
        
        Synchro__c latestEntry = opportunitiesFromWeb.get(opportunitiesFromWeb.size()-1);
        
        
        
        String oppsToBeUploaded = latestEntry.ToBeDeletedFromRelay__c;
        String accIdsForDeletionFromPhpSite = OpportunityUpsert.parseDataFromHttpConn(oppsToBeUploaded);
        
        HttpContactInsert.sendContents(accIdsForDeletionFromPhpSite, URL);
        
        return null;  
    }
    
    

    public Object startRequest() {

      Continuation con = new Continuation(30);
      con.continuationMethod='processResponse';

      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL);

      this.requestLabel = con.addHttpRequest(req);
      

      return con;  
    }
    

    public Object processResponse() {   

      HttpResponse response = Continuation.getResponse(this.requestLabel);
      String httpResponse = response.getBody();
      String[] httpSplit = httpResponse.split('\n');
        Boolean isTHereNewEntries = (httpSplit.size()>1);
       this.result = (isTHereNewEntries) ? (httpSplit.size()-1) + ' darab Opportunity importálható' :
        'Nincs Opportunity, ';
      
        if(isTHereNewEntries){
      Synchro__c syncItem = new Synchro__c();
       syncItem.Name = 'Imported from webDomain: ' + String.valueof(DateTime.now().getTime());
       syncItem.ToBeDeletedFromRelay__c = response.getBody();
        insert syncItem;
    }

      return null;
    }
}