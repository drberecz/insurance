@isTest
public class TestTaskFromCase {

    
    public static final String TASK_PRIORITY_LOW = 'Low';    
    public static final String TASK_PRIORITY_MEDIUM = 'Medium';    
    public static final String TASK_PRIORITY_HIGH = 'High';  
    
    
    @isTest
    public static void checkCasePriorityPicklistValues(){
        
        Integer NUM_OF_PICKLIST_VALUES_ALLOWED = 3;
        
        List<String> stringListFromEnumVals = TaskFromCase.getPickListValuesIntoList();

        System.assertEquals(NUM_OF_PICKLIST_VALUES_ALLOWED, stringListFromEnumVals.size());       
        System.assertEquals(TASK_PRIORITY_LOW, stringListFromEnumVals.get(2));        
        System.assertEquals(TASK_PRIORITY_MEDIUM, stringListFromEnumVals.get(1));   
        System.assertEquals(TASK_PRIORITY_HIGH, stringListFromEnumVals.get(0));           
    }
    
    
	@isTest
    public static void test_Low_Priority_CreateTasks(){
        
    Integer NUM_OF_CASES = 10;
        
    List<Case>  caseList= caseFactory(NUM_OF_CASES);
        for(Case c : caseList){
            c.Priority = TASK_PRIORITY_LOW;

        }
    insert caseList;
           
    Set<Id> caseIdSet = new Set<Id>();
        for(Case c : caseList){
            caseIdSet.add(c.Id);
        }

        
    List<Task> taskList = [SELECT WhatId, Description FROM Task WHERE WhatId IN :caseIdSet];
        
        
        System.assertEquals(taskList.size(), caseList.size());
        
    }    


	@isTest
    public static void test_ActivationDate_Medium_Priority(){
        
    Integer NUM_OF_CASES = 15;
        
    List<Case>  caseList= caseFactory(NUM_OF_CASES);
        for(Case c : caseList){
            c.Priority = TASK_PRIORITY_MEDIUM;

        }
    insert caseList;
           
    Set<Id> caseIdSet = extractCaseIds(caseList);      
    List<Task> taskList = [SELECT WhatId, Description,ActivityDate FROM Task WHERE WhatId IN :caseIdSet];
        
        Date todaysDate = System.today();
        
        Integer counter = 0;
        for(Case c : caseList){ 
        	System.assertEquals(ConstTask.DEADLINE_DAYS_MEDIUM_PRIORITY, todaysDate.daysBetween(taskList.get(counter++).ActivityDate));
        }
    }    



    
	@isTest
    public static void test_ActivationDate_Alternating_Priority(){
        
    Integer NUM_OF_CASES = 15;
    
    Integer counter = 0;
    List<Case>  caseList= caseFactory(NUM_OF_CASES);
        for(Case c : caseList){
            c.Priority = generateSequenceOfPriorityStrings(Math.mod(counter++,3));

        }
    insert caseList;
           
    Set<Id> caseIdSet = extractCaseIds(caseList);         
    List<Task> taskList = [SELECT WhatId, Description,ActivityDate FROM Task WHERE WhatId IN :caseIdSet];
        
    Date todaysDate = System.today();
        
    counter = 0;
    for(Case c : caseList){
        
        Integer expectedDeadlineDays;
        
        String CasePriority = c.Priority;
        
        switch on CasePriority{          
            when 'Low'{
                expectedDeadlineDays = ConstTask.DEADLINE_DAYS_LOW_PRIORITY;
            }
            when 'Medium'{
                expectedDeadlineDays = ConstTask.DEADLINE_DAYS_MEDIUM_PRIORITY;
            }
            when 'High'{
                expectedDeadlineDays = ConstTask.DEADLINE_DAYS_HIGH_PRIORITY;
            }            
            when else{
                expectedDeadlineDays = ConstTask.DEADLINE_DAYS_DEFAULT;              
            }
        }
        
    	System.assertEquals(expectedDeadlineDays, todaysDate.daysBetween(taskList.get(counter++).ActivityDate));
     }
    }    
    
    
    
    
    

    public static String generateSequenceOfPriorityStrings(Integer divisionOp){
        
        switch on divisionOp {
            
            when 0{
                return TASK_PRIORITY_LOW;
            }
            when 1{
                return TASK_PRIORITY_MEDIUM;
            }
            when 2{
                return TASK_PRIORITY_HIGH;
            }
        }
        return null;
        
    }
    


    
    
    public static List<Case> caseFactory(Integer numOfCases){
        
        List<Case> caseList = new List<Case>();
        
        for(Integer i=1; i <= numOfCases; i++){
            
            Case c = new Case(Description = 'testDescription'+i,
                             origin = 'Web', subject = 'testSubject'+i,
                             status = 'Not Started'
                             );
            c.Priority = 'Low';
            caseList.add(c);
            
        }
        return caseList;
    }

    
    public static Set<Id> extractCaseIds(List<Case> caseList){
       
        Set<Id> caseIdSet = new Set<Id>();
        for(Case c : caseList){
            caseIdSet.add(c.Id);
        }
        return caseIdSet;
        
    }
    
    
    
    
    
    
    
    
    
}