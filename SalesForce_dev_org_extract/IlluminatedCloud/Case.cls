// Generated by Illuminated Cloud on Mon Jan 20 20:25:46 CET 2020. Do not edit.

global class Case extends SObject 
{
    global Account Account;
    global Id AccountId;
    global List<ActivityHistory> ActivityHistories;
    global Asset Asset;
    global Id AssetId;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global BusinessHours BusinessHours;
    global Id BusinessHoursId;
    global List<CaseComment> CaseComments;
    global List<CaseContactRole> CaseContactRoles;
    global String CaseNumber;
    global List<Case> Cases;
    global List<CaseSolution> CaseSolutions;
    global Datetime ClosedDate;
    global List<CombinedAttachment> CombinedAttachments;
    global String Comments;
    global Contact Contact;
    global Email ContactEmail;
    global String ContactFax;
    global Id ContactId;
    global String ContactMobile;
    global String ContactPhone;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Description;
    global List<EmailMessage> EmailMessages;
    global List<EmailMessage> Emails;
    global List<Event> Events;
    global List<CaseFeed> Feeds;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global ContentVersion FirstPublishLocation;
    global List<CaseHistory> Histories;
    global Boolean IsClosed;
    global Boolean IsClosedOnCreate;
    global Boolean IsDeleted;
    global Boolean IsEscalated;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Case MasterRecord;
    global Id MasterRecordId;
    global List<MessagingSession> MessagingSessions;
    global List<OpenActivity> OpenActivities;
    global String Origin;
    global SObject Owner;
    global Id OwnerId;
    global Case Parent;
    global Id ParentId;
    /**
     * <h3>Potential Liability</h3>
     * <h3>Field Attributes</h3>
     * <table border="0" valign="top">
     * <tr><td><b>Data Type:</b></td><td>Picklist</td></tr>
     * <tr><td><b>Required:</b></td><td>false</td></tr>
     * <tr><td><b>External ID:</b></td><td>false</td></tr>
     * </table>
     * <h3>Picklist Values</h3>
     * <table border="0" valign="top">
     * <tr><td><b>API Name</b></td><td><b>Label</b></td><td><b>Description</b></td><td><b>Default</b></td><td><b>Active</b></td></tr>
     * <tr><td><code>No</code></td><td>No</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>Yes</code></td><td>Yes</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * </table>
     */
    global String PotentialLiability__c;
    global String Priority;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    /**
     * <h3>Product</h3>
     * <h3>Field Attributes</h3>
     * <table border="0" valign="top">
     * <tr><td><b>Data Type:</b></td><td>Picklist</td></tr>
     * <tr><td><b>Required:</b></td><td>false</td></tr>
     * <tr><td><b>External ID:</b></td><td>false</td></tr>
     * </table>
     * <h3>Picklist Values</h3>
     * <table border="0" valign="top">
     * <tr><td><b>API Name</b></td><td><b>Label</b></td><td><b>Description</b></td><td><b>Default</b></td><td><b>Active</b></td></tr>
     * <tr><td><code>GC1040</code></td><td>GC1040</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC1060</code></td><td>GC1060</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC3020</code></td><td>GC3020</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC3040</code></td><td>GC3040</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC3060</code></td><td>GC3060</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC5020</code></td><td>GC5020</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC5040</code></td><td>GC5040</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC5060</code></td><td>GC5060</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * <tr><td><code>GC1020</code></td><td>GC1020</td><td>&nbsp;</td><td>false</td><td>false</td></tr>
     * </table>
     */
    global String Product__c;
    global String Reason;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global ContentDistribution RelatedRecord;
    global EventRelationChangeEvent Relation;
    global List<CaseShare> Shares;
    global SObjectType SObjectType;
    global SObject Source;
    global Id SourceId;
    global String Status;
    global String Subject;
    global String SuppliedCompany;
    global Email SuppliedEmail;
    global String SuppliedName;
    global String SuppliedPhone;
    global Datetime SystemModstamp;
    global List<Task> Tasks;
    global List<CaseTeamMember> TeamMembers;
    global List<CaseTeamTemplateRecord> TeamTemplateRecords;
    global List<TopicAssignment> TopicAssignments;
    global String Type;
    global EventChangeEvent What;

    global Case()
    {
    }
}